package com.curve.microforum;

import com.curve.microforum.config.AppConfig;
import com.curve.microforum.dao.ThreadDao;
import com.curve.microforum.model.Thread;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.UUID;

@Testcontainers
@ContextConfiguration(classes = AppConfig.class)
@SpringBootTest()
class MicroForumApplicationTests {

	@Container
	public static PostgreSQLContainer container = new PostgreSQLContainer("postgres")
			.withDatabaseName("micro-forum")
			.withUsername("postgres")
			.withPassword("0004ed51ca8d");

	@Autowired
	private ThreadDao postgresThreadDao;

	@DynamicPropertySource
	static void properties(DynamicPropertyRegistry registry) {
		registry.add("spring.datasource.url", container::getJdbcUrl);
		registry.add("spring.datasource.username", container::getUsername);
		registry.add("spring.datasource.password", container::getPassword);
	}

	@Test
	void contextLoads() {
		Thread thread = new Thread(UUID.randomUUID(), "Title", "text", new String[]{"tag1"}, 0);
		postgresThreadDao.add(thread);
		Thread found = postgresThreadDao.findById(thread.getId()).get();

		Assertions.assertEquals(thread.getTitle(), found.getTitle());
	}

}
