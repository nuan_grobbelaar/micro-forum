create table thread(
    id UUID primary key not null,
    entity jsonb not null
);

create or replace procedure v1_thread_save(_id uuid, _entity jsonb)
as $$
begin
    insert into "thread" (id, entity)
    values(_id, _entity)
    on conflict on constraint thread_pkey do update set
        entity = EXCLUDED.entity;
end;
$$ language plpgsql
security definer;