package com.curve.microforum.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.UUID;

public class Thread {

    private final String JSON_ID_FIELD = "id";
    private final String JSON_TITLE_FIELD = "title";
    private final String JSON_TEXT_BODY_FIELD = "textBody";
    private final String JSON_TAGS_FIELD = "tags";
    private final String JSON_LIKES_FIELD = "likes";

    private final UUID id;
    private String title;
    private String textBody;
    private String[] tags;
    private long likes;

    public Thread(
            @JsonProperty(JSON_ID_FIELD) UUID id,
            @JsonProperty(JSON_TITLE_FIELD) String title,
            @JsonProperty(JSON_TEXT_BODY_FIELD) String textBody,
            @JsonProperty(JSON_TAGS_FIELD) String[] tags,
            @JsonProperty(JSON_LIKES_FIELD) long likes
    ) {
        this.id = id;
        this.title = title;
        this.textBody = textBody;
        this.tags = tags;
        this.likes = likes;
    }

    public Thread(UUID id, Thread thread) {
        this.id = id;
        this.title = thread.title;
        this.textBody = thread.textBody;
        this.tags = thread.tags;
        this.likes = thread.likes;
    }

    public UUID getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getTextBody() {
        return textBody;
    }

    public String[] getTags() {
        return tags;
    }

    public long getLikes() {
        return likes;
    }

    public void upvote() {
        this.likes++;
    }

    public void downvote() {
        this.likes--;
    }
}
