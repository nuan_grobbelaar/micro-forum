package com.curve.microforum;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
public class MicroForumApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroForumApplication.class, args);
	}

}
