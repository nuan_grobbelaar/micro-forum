package com.curve.microforum.service;

import com.curve.microforum.dao.ThreadDao;
import com.curve.microforum.model.Thread;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ThreadService {

    private final ThreadDao threadDao;

    @Autowired
    public ThreadService(@Qualifier("threadDao") ThreadDao threadDao) {
        this.threadDao = threadDao;
    }

    public Thread add(Thread thread) {
        return threadDao.add(thread);
    }

    public List<Thread> findAll() {
        return threadDao.findAll();
    }

    public Optional<Thread> findById(UUID id) {
        return  threadDao.findById(id);
    }

    public void upvote(UUID id) {
        Thread thread = threadDao.findById(id).orElseThrow();
        thread.upvote();
        threadDao.add(thread);
    }

    public void downvote(UUID id) {
        Thread thread = threadDao.findById(id).orElseThrow();
        thread.downvote();
        threadDao.add(thread);
    }
}
