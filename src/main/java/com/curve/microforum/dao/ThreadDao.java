package com.curve.microforum.dao;

import com.curve.microforum.model.Thread;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository("threadDao")
public class ThreadDao implements DataAccessFacade<Thread> {

    private final String QRY_FIND = "select entity from thread";
    private final String QRY_FIND_BY_ID = QRY_FIND + " where id = ?";
    private final String QRY_SAVE = "call v1_thread_save(?::uuid, ?::jsonb)";

    private static final ObjectMapper objectMapper = new ObjectMapper();

    private final JdbcTemplate jdbcTemplate;

    private static final Thread itemFactory(ResultSet rs, int index) {
        try {
            return objectMapper
                    .readValue(rs.getString(1), Thread.class);
        } catch (JsonProcessingException | SQLException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Autowired
    public ThreadDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Thread insert(UUID id, Thread thread) {
        try {
            jdbcTemplate.update(QRY_SAVE, id, objectMapper.writeValueAsString(new Thread(id, thread)));
            return findById(id).orElseThrow();
        } catch (JsonProcessingException e) {
            throw new RuntimeException();
        }
    }

    @Override
    public Thread add(Thread thread) {
        return insert(thread.getId() == null ? UUID.randomUUID() : thread.getId(), thread);
    }

    @Override
    public List<Thread> findAll() {
        return jdbcTemplate.query(QRY_FIND, ThreadDao::itemFactory);
    }

    @Override
    public Optional<Thread> findById(UUID id) {
        Thread t = jdbcTemplate.queryForObject(
                QRY_FIND_BY_ID,
                new Object[]{id},
                new int[]{Types.OTHER},
                ThreadDao::itemFactory);

        return Optional.ofNullable(t);
    }


}
