package com.curve.microforum.dao;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface DataAccessFacade<T> {

    T insert(UUID id, T t);

    default T add(T t) {return insert(UUID.randomUUID(), t);}

    List<T> findAll();

    Optional<T> findById(UUID id);

}
