package com.curve.microforum.api;

import com.curve.microforum.model.Thread;
import com.curve.microforum.service.ThreadService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("api/v1/thread")
public class ThreadController {

    private final static Logger LOGGER = LoggerFactory.getLogger(ThreadController.class);

    private final ThreadService threadService;

    @Autowired
    public ThreadController(ThreadService threadService) {
        this.threadService = threadService;
    }

    @PostMapping
    public void addUser(@RequestBody Thread thread) {
        threadService.add(thread);
    }

    @GetMapping
    public List<Thread> findAll(@RequestParam Map<String, String> params) {
        return threadService.findAll();
    }

    @GetMapping(path = "{id}")
    public Optional<Thread> findById(@PathVariable("id") UUID id) {
        return threadService.findById(id);
    }

    @GetMapping("/upvote/{id}")
    public void upvote(@PathVariable("id") UUID id) {
        threadService.upvote(id);
    }

    @GetMapping("/downvote/{id}")
    public void downvote(@PathVariable("id") UUID id) {
        threadService.downvote(id);
    }
}
