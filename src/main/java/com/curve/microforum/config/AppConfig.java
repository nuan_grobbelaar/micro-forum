package com.curve.microforum.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.curve.microforum")
public class AppConfig {
}
